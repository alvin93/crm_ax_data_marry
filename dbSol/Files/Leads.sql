SELECT DISTINCT [EMPLOYEE] = CASE 
						WHEN (S.MAINCONTACT IS NOT NULL OR S.MAINCONTACT <> '')
						AND (R.KAMID IS  NULL OR R.KAMID = '')
						AND (Q.SALESRESPONSIBLE IS NULL OR Q.SALESRESPONSIBLE = '' )THEN (Select S.MAINCONTACT from CRM_AXUsers where EMPLOYEEID = S.MAINCONTACT) 
						
						WHEN (R.KAMID IS NOT  NULL OR R.KAMID <> '')
						AND (S.MAINCONTACT IS  NULL OR S.MAINCONTACT = ''	)					
						AND (Q.SALESRESPONSIBLE IS NULL OR Q.SALESRESPONSIBLE = '') THEN (Select  R.KAMID from CRM_AXUsers where EMPLOYEEID = R.KAMID) 

						WHEN (Q.SALESRESPONSIBLE IS NOT NULL OR Q.SALESRESPONSIBLE <> '')
						AND( S.MAINCONTACT IS  NULL OR S.MAINCONTACT = '')
						AND (R.KAMID IS  NULL OR R.KAMID = '' )THEN (Select Fullname from CRM_AXUsers where EMPLOYEEID = Q.SALESRESPONSIBLE) 
						
						WHEN O.ownerid IS NOT NULL 
                             AND Q.salesresponsible IS NULL 
                             AND R.kamid IS NULL 
                             AND S.maincontact IS NULL THEN (Select Fullname from CRM_AXUsers where EMPLOYEEID = u.EMPLOYEEID)
						
						WHEN L.ownerid IS NOT NULL
							 AND O.ownerid IS NULL 
                             AND Q.salesresponsible IS NULL 
                             AND R.kamid IS NULL 
                             AND S.maincontact IS NULL THEN (Select Fullname from CRM_AXUsers where EMPLOYEEID = u.EMPLOYEEID )
				 			
					
					
                  END,
	   L.[companyname]
      ,L.[leadid]
      ,L.[ownerid]
      ,L.[ownerid_Name]
      ,L.[parentcontactid]
      ,L.[plain_parentcontactid]
      ,L.[parentaccountid]
      ,L.[plain_parentaccountid]
      ,L.[mobilephone]
      ,L.[yomifullname]
      ,L.[fullname]
      ,L.[hgh_lead_businesssite]
      ,L.[hgh_lead_businesssite_Name]
      ,L.[address1_city]
      ,L.[customerid]
      ,L.[customerid_Name]
      ,L.[createdon]
      ,L.[modifiedon]
      ,L.[createdby]
      ,L.[createdby_Name]
      ,L.[modifiedby]
      ,L.[modifiedby_Name]
      ,L.[statuscode]
      ,L.[prioritycode]
      ,L.[hgh_territoryid]
      ,L.[hghterrirotyid_Name]
      ,L.[qualifyingopportunityid]
      ,L.[merged]
      ,L.[emailaddress1]
      ,L.[confirminterest]
      ,L.[exchangerate]
      ,L.[decisionmaker]
      ,L.[owningbusinessunit]
      ,L.[owninguser]
      ,L.[address1_shippingmethodcode]
      ,L.[address1_composite]
      ,L.[lastname]
      ,L.[donotpostalmail]
      ,L.[donotphone]
      ,L.[preferredcontactmethodcode]
      ,L.[stageid]
      ,L.[firstname]
      ,L.[traversedpath]
      ,L.[evaluatefit]
      ,L.[donotemail]
      ,L.[address2_shippingmethodcode]
      ,L.[subject]
      ,L.[processid]
      ,L.[msdyn_ordertype]
      ,L.[donotfax]
      ,L.[donotsendmm]
      ,L.[address1_line1]
      ,L.[telephone1]
      ,L.[leadqualitycode]
      ,L.[transactioncurrencyid]
      ,L.[address1_addresstypecode]
      ,L.[donotbulkemail]
      ,L.[followemail]
      ,L.[address2_addresstypecode]
      ,L.[salesstagecode]
      ,L.[hgh_segmentid]
      ,L.[participatesinworkflow]
      ,L.[statecode]
      ,L.[address2_addressid]
      ,L.[hgh_sectorid]
      ,L.[estimatedamount]
      ,L.[estimatedvalue]
      ,L.[industrycode]
      ,L.[leadsourcecode]
      ,L.[budgetamount]
      ,L.[hgh_territoryid_Name]
      ,L.[revenue]
      ,L.[sic]
  FROM [CRM_OnlineData].[dbo].[Leads] L
  LEFT JOIN   [CRM_OnlineData].[dbo].[opportunities] o      
              ON O.originatingleadid = CONVERT(VARCHAR(50), L.leadid) 
       LEFT JOIN [SQLBI].HGH_BI.dbo.dimquotations Q  
              ON Q.quotationid = o.hgh_quoteid 
                 AND Q.dataareaid = 'eaz' 
       LEFT JOIN [SQLBI].HGH_BI.dbo.dimrentalorder R 
              ON R.rentalorder = Q.salesidref 
                 AND rentalorderkey like 'eaz%' 
       LEFT JOIN [SQLBI].Eazi_Ax_Production.dbo.smmbusreltable S 
              ON S.busrelaccount = Q.custaccount 
                 AND S.dataareaid = 'com' 
       LEFT JOIN crm_axusers U 
              ON U.crmguid = L.ownerid 
			 