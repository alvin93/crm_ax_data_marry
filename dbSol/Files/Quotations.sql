SELECT [EMPLOYEE] = CASE 
                      WHEN S.maincontact <> '' 
		     
			 THEN (SELECT fullname FROM crm_onlinedata.dbo.crm_axusers 
				   WHERE employeeid = s.maincontact) 
                   WHEN ( S.maincontact IS NULL OR S.maincontact = '' )  AND ( R.kamid IS NULL  OR R.kamid = '' ) 
      		     
				 THEN (SELECT fullname FROM crm_onlinedata.dbo.crm_axusers 
                    WHERE   employeeid = q.salesresponsible) 
                    WHEN ( S.maincontact IS NULL OR S.maincontact = '' ) AND ( R.kamid IS NOT NULL OR R.kamid <> '' )  
                    THEN CASE 
                         WHEN R.kamid IS NOT NULL OR R.kamid <> '' THEN r.kamname 
                         END 
                    END, 
       Q.[quotationid], 
       Q.[quotationname], 
       Q.[quotationexpirydate], 
       Q.[quotationtype], 
       Q.[confirmdate], 
       Q.[quotationstatus], 
       Q.[quotationcategory], 
       Q.[deliveryname], 
       Q.[deliveryaddress], 
       Q.[deliverystreet], 
       Q.[deliveryzipcode], 
       Q.[deliverycity], 
       Q.[deliverycounty], 
       Q.[deliverystate], 
       Q.[deliverycountryregionid], 
       Q.[deliverydate], 
       Q.[salesresponsible], 
       Q.[inventlocationid], 
       Q.[dimension] 
       --,Q.[CUSTACCOUNT] 
       , 
       CASE 
         WHEN R.customeraccount <> '' 
               OR R.customeraccount IS NOT NULL THEN R.customeraccount 
         WHEN R.customeraccount = '' 
               OR R.customeraccount IS NULL THEN Q.custaccount 
         ELSE NULL 
       END AS 'CUSACCOUNT', 
       Q.[busrelaccount], 
       Q.[email], 
       Q.[reasonid], 
       Q.[salesidref], 
       Q.[salestaker], 
       Q.[salesoriginid], 
       Q.[quotationfollowupdate], 
       Q.[invoiceaccount], 
       Q.[estimate], 
       Q.[opportunityid], 
       Q.[rorvenuecode], 
       Q.[rorcalendarid], 
       Q.[invinvoiceprofileid], 
       Q.[rorpriceratecode] 
       -- ,Q.[CREATEDDATETIME] 
       , 
       CASE 
         WHEN R.dateonhire IS NOT NULL 
               OR R.dateonhire <> '' THEN Cast(R.dateonhire AS DATE) 
         WHEN R.dateonhire IS NULL 
               OR R.dateonhire = '' THEN Cast(Q.CREATEDDATETIME AS DATE) 
         ELSE NULL 
       END AS 'THEDATE', 
       Q.[dataareaid] 
FROM  [SQLBI].HGH_BI.dbo.[dimquotations] Q 
       LEFT JOIN [SQLBI].HGH_BI.dbo.dimrentalorder R 
              ON Q.salesidref = R.rentalorder 
                 AND Q.dataareaid = 'eaz' 
       LEFT JOIN [SQLBI].[Eazi_Ax_Production].dbo.[SMMBUSRELTABLE] S 
              ON S.busrelaccount = Q.custaccount 
                 AND S.dataareaid = 'com' 			 

WHERE  R.rentalorderkey LIKE 'eaz%'       AND Year(R.dateonhire) >= 2017 


