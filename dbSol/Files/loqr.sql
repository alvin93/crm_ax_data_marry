USE [CRM_OnlineData]
GO
/****** Object:  Table [dbo].[Accounts]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Accounts](
	[address2_addresstypecode] [nvarchar](max) NULL,
	[hgh_sectorid] [nvarchar](max) NULL,
	[merged] [bit] NULL,
	[accountnumber] [nvarchar](max) NULL,
	[statecode] [nvarchar](max) NULL,
	[emailaddress1] [nvarchar](max) NULL,
	[exchangerate] [decimal](18, 0) NULL,
	[address1_composite] [nvarchar](max) NULL,
	[name] [nvarchar](max) NULL,
	[opendeals] [nvarchar](max) NULL,
	[address1_county] [nvarchar](max) NULL,
	[modifiedon] [datetime] NULL,
	[owninguser] [nvarchar](max) NULL,
	[primarycontactid] [nvarchar](max) NULL,
	[hgh_businesssite] [nvarchar](max) NULL,
	[openrevenue_state] [nvarchar](max) NULL,
	[donotpostalmail] [bit] NULL,
	[accountratingcode] [nvarchar](max) NULL,
	[marketingonly] [bit] NULL,
	[donotphone] [bit] NULL,
	[preferredcontactmethodcode] [nvarchar](max) NULL,
	[territoryid] [nvarchar](max) NULL,
	[ownerid] [nvarchar](max) NULL,
	[hgh_partyid] [nvarchar](max) NULL,
	[customersizecode] [nvarchar](max) NULL,
	[hgh_account_stateprovince] [nvarchar](max) NULL,
	[openrevenue_date] [datetime] NULL,
	[openrevenue_base] [money] NULL,
	[donotemail] [bit] NULL,
	[opendeals_state] [nvarchar](max) NULL,
	[address2_shippingmethodcode] [nvarchar](max) NULL,
	[address1_addressid] [uniqueidentifier] NULL,
	[address2_freighttermscode] [nvarchar](max) NULL,
	[statuscode] [nvarchar](max) NULL,
	[createdon] [datetime] NULL,
	[msdyn_travelchargetype] [nvarchar](max) NULL,
	[creditlimit] [money] NULL,
	[address1_stateorprovince] [nvarchar](max) NULL,
	[originatingleadid] [nvarchar](max) NULL,
	[openrevenue] [money] NULL,
	[donotsendmm] [bit] NULL,
	[donotfax] [bit] NULL,
	[donotbulkpostalmail] [bit] NULL,
	[address1_country] [nvarchar](max) NULL,
	[address1_line1] [nvarchar](max) NULL,
	[creditonhold] [bit] NULL,
	[telephone1] [nvarchar](max) NULL,
	[owningbusinessunit] [nvarchar](max) NULL,
	[transactioncurrencyid] [nvarchar](max) NULL,
	[accountid] [uniqueidentifier] NULL,
	[donotbulkemail] [bit] NULL,
	[creditlimit_base] [money] NULL,
	[modifiedby] [nvarchar](max) NULL,
	[followemail] [bit] NULL,
	[businesstypecode] [nvarchar](max) NULL,
	[hgh_account_country] [nvarchar](max) NULL,
	[createdby] [nvarchar](max) NULL,
	[address2_latitude] [nvarchar](max) NULL,
	[address1_name] [nvarchar](max) NULL,
	[territorycode] [nvarchar](max) NULL,
	[hgh_segmentid] [nvarchar](max) NULL,
	[fax] [nvarchar](max) NULL,
	[msdyn_taxexempt] [bit] NULL,
	[participatesinworkflow] [bit] NULL,
	[accountclassificationcode] [nvarchar](max) NULL,
	[address2_name] [nvarchar](max) NULL,
	[address2_addressid] [uniqueidentifier] NULL,
	[hgh_accounttype] [nvarchar](max) NULL,
	[address2_longitude] [nvarchar](max) NULL,
	[shippingmethodcode] [nvarchar](max) NULL,
	[opendeals_date] [datetime] NULL,
	[industrycode] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BI_DateDimension]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BI_DateDimension](
	[TheDate] [datetime] NULL,
	[Year] [int] NULL,
	[Month] [int] NULL,
	[Week] [int] NULL,
	[Day] [int] NULL,
	[MonthName] [varchar](100) NULL,
	[DayName] [varchar](100) NULL,
	[DayOfYear] [int] NULL,
	[WeekDay] [int] NULL,
	[FriendlyDate] [varchar](200) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BusinessUnits]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessUnits](
	[address2_shippingmethodcode] [nvarchar](max) NULL,
	[address1_addressid] [uniqueidentifier] NULL,
	[divisionname] [nvarchar](max) NULL,
	[createdon] [datetime] NULL,
	[workflowsuspended] [bit] NULL,
	[address1_addresstypecode] [nvarchar](max) NULL,
	[businessunitid] [uniqueidentifier] NULL,
	[parentbusinessunitid] [nvarchar](max) NULL,
	[name] [nvarchar](max) NULL,
	[address1_shippingmethodcode] [nvarchar](max) NULL,
	[address2_addresstypecode] [nvarchar](max) NULL,
	[transactioncurrencyid] [nvarchar](max) NULL,
	[exchangerate] [money] NULL,
	[modifiedby] [nvarchar](max) NULL,
	[modifiedon] [datetime] NULL,
	[organizationid] [nvarchar](max) NULL,
	[createdby] [nvarchar](max) NULL,
	[isdisabled] [bit] NULL,
	[address2_addressid] [uniqueidentifier] NULL,
	[inheritancemask] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contacts]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacts](
	[mobilephone] [nvarchar](max) NULL,
	[modifiedon] [datetime] NULL,
	[ownerid] [nvarchar](max) NULL,
	[contactid] [uniqueidentifier] NULL,
	[originatingleadid] [nvarchar](max) NULL,
	[yomifullname] [nvarchar](max) NULL,
	[fullname] [nvarchar](max) NULL,
	[createdon] [datetime] NULL,
	[modifiedby] [nvarchar](max) NULL,
	[createdby] [nvarchar](max) NULL,
	[parentcustomerid] [nvarchar](max) NULL,
	[address1_composite] [nvarchar](max) NULL,
	[customertypecode] [nvarchar](max) NULL,
	[transactioncurrencyid] [nvarchar](max) NULL,
	[merged] [bit] NULL,
	[territorycode] [nvarchar](max) NULL,
	[emailaddress1] [nvarchar](max) NULL,
	[haschildrencode] [nvarchar](max) NULL,
	[exchangerate] [money] NULL,
	[preferredappointmenttimecode] [nvarchar](max) NULL,
	[isbackofficecustomer] [bit] NULL,
	[owninguser] [nvarchar](max) NULL,
	[lastname] [nvarchar](max) NULL,
	[donotpostalmail] [bit] NULL,
	[marketingonly] [bit] NULL,
	[donotphone] [bit] NULL,
	[preferredcontactmethodcode] [nvarchar](max) NULL,
	[educationcode] [nvarchar](max) NULL,
	[customersizecode] [nvarchar](max) NULL,
	[firstname] [nvarchar](max) NULL,
	[donotemail] [bit] NULL,
	[address2_shippingmethodcode] [nvarchar](max) NULL,
	[address1_addressid] [uniqueidentifier] NULL,
	[address2_freighttermscode] [nvarchar](max) NULL,
	[statuscode] [nvarchar](max) NULL,
	[donotsendmm] [bit] NULL,
	[donotfax] [bit] NULL,
	[leadsourcecode] [nvarchar](max) NULL,
	[address1_line1] [nvarchar](max) NULL,
	[hgh_isoperator] [bit] NULL,
	[creditonhold] [bit] NULL,
	[telephone1] [nvarchar](max) NULL,
	[address3_addressid] [uniqueidentifier] NULL,
	[donotbulkemail] [bit] NULL,
	[followemail] [bit] NULL,
	[shippingmethodcode] [nvarchar](max) NULL,
	[donotbulkpostalmail] [bit] NULL,
	[address2_addresstypecode] [nvarchar](max) NULL,
	[participatesinworkflow] [bit] NULL,
	[statecode] [nvarchar](max) NULL,
	[owningbusinessunit] [nvarchar](max) NULL,
	[address2_addressid] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CRM_AXUsers]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CRM_AXUsers](
	[EMPLOYEEID] [nvarchar](50) NULL,
	[FULLNAME] [nvarchar](100) NULL,
	[CRMGUID] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CRMAXFactTable]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CRMAXFactTable](
	[EMPLOYEEID] [nvarchar](max) NULL,
	[leadid] [uniqueidentifier] NULL,
	[opportunityid] [uniqueidentifier] NULL,
	[quotationid] [nvarchar](50) NULL,
	[rentalorderkey] [nvarchar](25) NULL,
	[originatingleadid] [nvarchar](max) NULL,
	[THEDATE] [date] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[hgh_businesssite]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hgh_businesssite](
	[statecode] [nvarchar](max) NULL,
	[hgh_zippostalcode] [nvarchar](max) NULL,
	[statuscode] [nvarchar](max) NULL,
	[createdon] [datetime] NULL,
	[hgh_street] [nvarchar](max) NULL,
	[hgh_city] [nvarchar](max) NULL,
	[ownerid] [nvarchar](max) NULL,
	[hgh_originatedleadid] [nvarchar](max) NULL,
	[hgh_businesssiteid] [uniqueidentifier] NULL,
	[hgh_stateprovince_businesssite] [nvarchar](max) NULL,
	[hgh_name] [nvarchar](max) NULL,
	[hgh_territoryid] [nvarchar](max) NULL,
	[hgh_transportunitkm_txt] [nvarchar](max) NULL,
	[modifiedby] [nvarchar](max) NULL,
	[hgh_latitude] [nvarchar](max) NULL,
	[modifiedon] [datetime] NULL,
	[hgh_stateprovince] [nvarchar](max) NULL,
	[hgh_longitude] [nvarchar](max) NULL,
	[createdby] [nvarchar](max) NULL,
	[owningbusinessunit] [nvarchar](max) NULL,
	[hgh_country] [nvarchar](max) NULL,
	[owninguser] [nvarchar](max) NULL,
	[hgh_businesssite_country] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Leads]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Leads](
	[companyname] [nvarchar](max) NULL,
	[leadid] [uniqueidentifier] NULL,
	[ownerid] [nvarchar](max) NULL,
	[ownerid_Name] [nvarchar](100) NULL,
	[parentcontactid] [nvarchar](max) NULL,
	[plain_parentcontactid] [nvarchar](100) NULL,
	[parentaccountid] [nvarchar](max) NULL,
	[plain_parentaccountid] [nvarchar](100) NULL,
	[mobilephone] [nvarchar](max) NULL,
	[yomifullname] [nvarchar](max) NULL,
	[fullname] [nvarchar](max) NULL,
	[hgh_lead_businesssite] [nvarchar](100) NULL,
	[hgh_lead_businesssite_Name] [nvarchar](100) NULL,
	[address1_city] [nvarchar](100) NULL,
	[customerid] [nvarchar](100) NULL,
	[customerid_Name] [nvarchar](100) NULL,
	[createdon] [datetime] NULL,
	[modifiedon] [datetime] NULL,
	[createdby] [nvarchar](max) NULL,
	[createdby_Name] [nvarchar](100) NULL,
	[modifiedby] [nvarchar](max) NULL,
	[modifiedby_Name] [nvarchar](100) NULL,
	[statuscode] [nvarchar](max) NULL,
	[prioritycode] [nvarchar](max) NULL,
	[hgh_territoryid] [nvarchar](max) NULL,
	[hghterrirotyid_Name] [nvarchar](100) NULL,
	[qualifyingopportunityid] [nvarchar](max) NULL,
	[merged] [bit] NULL,
	[emailaddress1] [nvarchar](max) NULL,
	[confirminterest] [bit] NULL,
	[exchangerate] [money] NULL,
	[decisionmaker] [bit] NULL,
	[owningbusinessunit] [nvarchar](max) NULL,
	[owninguser] [nvarchar](max) NULL,
	[address1_shippingmethodcode] [nvarchar](max) NULL,
	[address1_composite] [nvarchar](max) NULL,
	[lastname] [nvarchar](max) NULL,
	[donotpostalmail] [bit] NULL,
	[donotphone] [bit] NULL,
	[preferredcontactmethodcode] [nvarchar](max) NULL,
	[stageid] [uniqueidentifier] NULL,
	[firstname] [nvarchar](max) NULL,
	[traversedpath] [nvarchar](max) NULL,
	[evaluatefit] [bit] NULL,
	[donotemail] [bit] NULL,
	[address2_shippingmethodcode] [nvarchar](max) NULL,
	[subject] [nvarchar](max) NULL,
	[processid] [uniqueidentifier] NULL,
	[msdyn_ordertype] [nvarchar](max) NULL,
	[donotfax] [bit] NULL,
	[donotsendmm] [bit] NULL,
	[address1_line1] [nvarchar](max) NULL,
	[telephone1] [nvarchar](max) NULL,
	[leadqualitycode] [nvarchar](max) NULL,
	[transactioncurrencyid] [nvarchar](max) NULL,
	[address1_addresstypecode] [nvarchar](max) NULL,
	[donotbulkemail] [bit] NULL,
	[followemail] [bit] NULL,
	[address2_addresstypecode] [nvarchar](max) NULL,
	[salesstagecode] [nvarchar](max) NULL,
	[hgh_segmentid] [nvarchar](max) NULL,
	[participatesinworkflow] [bit] NULL,
	[statecode] [nvarchar](max) NULL,
	[address2_addressid] [uniqueidentifier] NULL,
	[hgh_sectorid] [nvarchar](max) NULL,
	[estimatedamount] [money] NULL,
	[estimatedvalue] [money] NULL,
	[industrycode] [nvarchar](100) NULL,
	[leadsourcecode] [nvarchar](100) NULL,
	[budgetamount] [money] NULL,
	[hgh_territoryid_Name] [nvarchar](100) NULL,
	[revenue] [money] NULL,
	[sic] [nvarchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Opportunities]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Opportunities](
	[opportunityid] [uniqueidentifier] NULL,
	[name] [nvarchar](max) NULL,
	[ownerid] [nvarchar](max) NULL,
	[ownerid_Name] [nvarchar](100) NULL,
	[hgh_City] [nvarchar](100) NULL,
	[hgh_branch] [nvarchar](100) NULL,
	[hgh_branch_Name] [nvarchar](100) NULL,
	[hgh_opportunity_sitename] [nvarchar](100) NULL,
	[createdon] [datetime] NULL,
	[createdby] [nvarchar](max) NULL,
	[hgh_rentalstartdate] [datetime] NULL,
	[createdby_Name] [nvarchar](100) NULL,
	[modifiedby] [uniqueidentifier] NULL,
	[modifiedby_Name] [nvarchar](100) NULL,
	[originatingleadid] [nvarchar](max) NULL,
	[originatingleadid_Name] [nvarchar](100) NULL,
	[hgh_quoteid] [nvarchar](255) NULL,
	[customerid] [nvarchar](100) NULL,
	[customerid_Name] [nvarchar](100) NULL,
	[parentcontactid] [nvarchar](100) NULL,
	[parentcontactid_Name] [nvarchar](100) NULL,
	[parentaccountid] [nvarchar](100) NULL,
	[parentaccountid_Name] [nvarchar](100) NULL,
	[transactioncurrencyid] [nvarchar](100) NULL,
	[transactioncurrencyid_Name] [nvarchar](100) NULL,
	[actualvalue] [money] NULL,
	[totalamount] [money] NULL,
	[actualvalue_base] [money] NULL,
	[hgh_axtotalprice_base] [money] NULL,
	[actualclosedate] [datetime] NULL,
	[processid] [varchar](max) NULL,
	[plain_processid] [nvarchar](100) NULL,
	[hgh_bussinesssiteid] [nvarchar](100) NULL,
	[hgh_bussinesssiteid_Name] [nvarchar](100) NULL,
	[hgh_ordercomments] [nvarchar](100) NULL,
	[discountpercentage] [nvarchar](100) NULL,
	[salesstage] [nvarchar](100) NULL,
	[stepname] [nvarchar](100) NULL,
	[filedebrief] [bit] NULL,
	[estimatedclosedate] [datetime] NULL,
	[pricelevelid] [nvarchar](100) NULL,
	[new_segment] [nvarchar](100) NULL,
	[finaldecisiondate] [datetime] NULL,
	[captureproposalfeedback] [bit] NULL,
	[new_sector] [nvarchar](100) NULL,
	[hgh_contractortype_security] [bit] NULL,
	[identifycompetitors] [bit] NULL,
	[hgh_axtotalprice] [money] NULL,
	[hgh_rentalenddate] [datetime] NULL,
	[decisionmaker] [bit] NULL,
	[hgh_adress1_composite] [nvarchar](100) NULL,
	[prioritycode] [nvarchar](100) NULL,
	[hgh_calendar] [nvarchar](100) NULL,
	[isrevenuesystemcalculated] [bit] NULL,
	[completeinternalreview] [bit] NULL,
	[hgh_contractortype_structuralsteel] [bit] NULL,
	[confirminterest] [bit] NULL,
	[presentproposal] [bit] NULL,
	[hgh_latitude] [nvarchar](100) NULL,
	[proposedsolution] [nvarchar](max) NULL,
	[hgh_contractortype_electrical] [bit] NULL,
	[hgh_contractortype_plumbing] [bit] NULL,
	[identifycustomercontacts] [bit] NULL,
	[hgh_contractortype_hvac] [bit] NULL,
	[stageid] [nvarchar](max) NULL,
	[traversedpath] [nvarchar](max) NULL,
	[evaluatefit] [bit] NULL,
	[totalamountlessfreight] [money] NULL,
	[totallineitemdiscountamount] [money] NULL,
	[hgh_contractortype] [nvarchar](max) NULL,
	[timezoneruleversionnumber] [nvarchar](max) NULL,
	[totaldiscountamount] [money] NULL,
	[statuscode] [nvarchar](100) NULL,
	[totalamountlessfreight_base] [money] NULL,
	[msdyn_ordertype] [nvarchar](max) NULL,
	[customerneed] [nvarchar](max) NULL,
	[totaltax_base] [money] NULL,
	[totallineitemamount_base] [money] NULL,
	[estimatedvalue] [money] NULL,
	[totalamount_base] [money] NULL,
	[developproposal] [bit] NULL,
	[purchaseprocess] [nvarchar](max) NULL,
	[currentsituation] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[modifiedon] [datetime] NULL,
	[resolvefeedback] [bit] NULL,
	[totaltax] [money] NULL,
	[totaldiscountamount_base] [money] NULL,
	[hgh_longitude] [nvarchar](max) NULL,
	[hgh_invoiceprofile] [nvarchar](max) NULL,
	[sendthankyounote] [bit] NULL,
	[hgh_contractortype_claddingroofing] [bit] NULL,
	[exchangerate] [decimal](18, 0) NULL,
	[estimatedvalue_base] [money] NULL,
	[hgh_contractortype_painting] [bit] NULL,
	[budgetamount_base] [money] NULL,
	[hgh_tequestforquote] [bit] NULL,
	[presentfinalproposal] [bit] NULL,
	[owninguser] [nvarchar](max) NULL,
	[hgh_contractortype_fireinstallation] [bit] NULL,
	[budgetamount] [money] NULL,
	[pricingerrorcode] [nvarchar](max) NULL,
	[hgh_contractortype_signage] [bit] NULL,
	[salesstagecode] [nvarchar](max) NULL,
	[totallineitemdiscountamount_base] [money] NULL,
	[hgh_contractortype_civils] [bit] NULL,
	[modifiedonbehalfby] [nvarchar](max) NULL,
	[purchasetimeframe] [nvarchar](max) NULL,
	[identifypursuitteam] [bit] NULL,
	[participatesinworkflow] [bit] NULL,
	[statecode] [nvarchar](max) NULL,
	[owningbusinessunit] [nvarchar](max) NULL,
	[pursuitdecision] [bit] NULL,
	[opportunityratingcode] [nvarchar](max) NULL,
	[totallineitemamount] [money] NULL,
	[completefinalproposal] [bit] NULL,
	[LeadExists] [bit] NOT NULL CONSTRAINT [DF__Opportuni__LeadE__2C3393D0]  DEFAULT ((0))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpportunityProducts]    Script Date: 2018-10-05 2:31:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpportunityProducts](
	[manualdiscountamount_base] [money] NULL,
	[owningbusinessunit] [uniqueidentifier] NULL,
	[priceperunit] [money] NULL,
	[productid] [nvarchar](max) NULL,
	[createdon] [datetime] NULL,
	[pricingerrorcode] [nvarchar](max) NULL,
	[ownerid] [nvarchar](max) NULL,
	[producttypecode] [nvarchar](max) NULL,
	[modifiedon] [datetime] NULL,
	[propertyconfigurationstatus] [nvarchar](max) NULL,
	[volumediscountamount_base] [money] NULL,
	[extendedamount_base] [money] NULL,
	[extendedamount] [money] NULL,
	[transactioncurrencyid] [nvarchar](max) NULL,
	[opportunityproductid] [uniqueidentifier] NULL,
	[exchangerate] [decimal](18, 0) NULL,
	[quantity] [decimal](18, 0) NULL,
	[opportunityid] [nvarchar](max) NULL,
	[modifiedby] [nvarchar](max) NULL,
	[baseamount_base] [money] NULL,
	[modifiedonbehalfby] [nvarchar](max) NULL,
	[baseamount] [money] NULL,
	[volumediscountamount] [money] NULL,
	[ispriceoverridden] [bit] NULL,
	[sequencenumber] [nvarchar](max) NULL,
	[uomid] [nvarchar](max) NULL,
	[createdby] [nvarchar](max) NULL,
	[isproductoverridden] [bit] NULL,
	[owninguser] [uniqueidentifier] NULL,
	[manualdiscountamount] [money] NULL,
	[opportunitystatecode] [nvarchar](max) NULL,
	[priceperunit_base] [money] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 2018-10-05 2:31:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[statecode] [nvarchar](max) NULL,
	[productid] [uniqueidentifier] NULL,
	[statuscode] [nvarchar](max) NULL,
	[iskit] [bit] NULL,
	[description] [nvarchar](max) NULL,
	[createdon] [datetime] NULL,
	[importsequencenumber] [nvarchar](max) NULL,
	[productnumber] [nvarchar](max) NULL,
	[msdyn_taxable] [bit] NULL,
	[producttypecode] [nvarchar](max) NULL,
	[name] [nvarchar](max) NULL,
	[price] [money] NULL,
	[isstockitem] [bit] NULL,
	[productstructure] [nvarchar](max) NULL,
	[defaultuomscheduleid] [nvarchar](max) NULL,
	[transactioncurrencyid] [nvarchar](max) NULL,
	[exchangerate] [decimal](18, 0) NULL,
	[pricelevelid] [nvarchar](max) NULL,
	[modifiedby] [nvarchar](max) NULL,
	[msdyn_fieldserviceproducttype] [nvarchar](max) NULL,
	[modifiedon] [datetime] NULL,
	[price_base] [money] NULL,
	[defaultuomid] [nvarchar](max) NULL,
	[quantitydecimal] [nvarchar](max) NULL,
	[createdby] [nvarchar](max) NULL,
	[organizationid] [nvarchar](max) NULL,
	[msdyn_converttocustomerasset] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SystemUsers]    Script Date: 2018-10-05 2:31:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemUsers](
	[systemuserid] [uniqueidentifier] NULL,
	[accessmode] [nvarchar](max) NULL,
	[firstname] [nvarchar](max) NULL,
	[issyncwithdirectory] [bit] NULL,
	[incomingemaildeliverymethod] [nvarchar](max) NULL,
	[domainname] [nvarchar](max) NULL,
	[userlicensetype] [nvarchar](max) NULL,
	[createdon] [datetime] NULL,
	[fullname] [nvarchar](max) NULL,
	[businessunitid] [nvarchar](max) NULL,
	[isintegrationuser] [bit] NULL,
	[caltype] [nvarchar](max) NULL,
	[modifiedon] [datetime] NULL,
	[defaultfilterspopulated] [bit] NULL,
	[outgoingemaildeliverymethod] [nvarchar](max) NULL,
	[emailrouteraccessapproval] [nvarchar](max) NULL,
	[lastname] [nvarchar](max) NULL,
	[setupuser] [bit] NULL,
	[organizationid] [uniqueidentifier] NULL,
	[defaultodbfoldername] [nvarchar](max) NULL,
	[isemailaddressapprovedbyo365admin] [bit] NULL,
	[invitestatuscode] [nvarchar](max) NULL,
	[isdisabled] [bit] NULL,
	[islicensed] [bit] NULL,
	[employeeid] [nvarchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TransactionCurrencies]    Script Date: 2018-10-05 2:31:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionCurrencies](
	[organizationid] [nvarchar](max) NULL,
	[currencyname] [nvarchar](max) NULL,
	[modifiedonbehalfby] [nvarchar](max) NULL,
	[createdonbehalfby] [nvarchar](max) NULL,
	[statecode] [nvarchar](max) NULL,
	[statuscode] [nvarchar](max) NULL,
	[createdby] [nvarchar](max) NULL,
	[isocurrencycode] [nvarchar](max) NULL,
	[currencysymbol] [nvarchar](max) NULL,
	[modifiedby] [nvarchar](max) NULL,
	[createdon] [datetime] NULL,
	[exchangerate] [decimal](18, 0) NULL,
	[modifiedon] [datetime] NULL,
	[currencyprecision] [nvarchar](max) NULL,
	[transactioncurrencyid] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  View [dbo].[vw_DateLink]    Script Date: 2018-10-05 2:31:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vw_DateLink]
/****** Script for SelectTopNRows command from SSMS  ******/
AS

SELECT        dateDim.TheDate
,dateDim.[Year]
      ,dateDim.[Month]
      ,dateDim.[Week]
      ,dateDim.[Day]
      ,dateDim.[MonthName]
      ,dateDim.[DayName]
      ,dateDim.[DayOfYear]
      ,dateDim.[WeekDay]
      ,dateDim.[FriendlyDate]
              ,dbo.Opportunities.opportunityid
                       ,dbo.Leads.leadid
                       ,dbo.DimQuotations.QUOTATIONID
                       ,dbo.DimRentalOrder.[RentalOrder]
                       ,dbo.DimRentalOrder.RentalOrderKey
FROM          dbo.BI_DateDimension as dateDim 
left JOIN     dbo.Opportunities ON CONVERT(date, dateDim.TheDate)  = CONVERT(date,dbo.Opportunities.createdon)
left JOIN     dbo.Leads ON CONVERT(date, dateDim.TheDate)  = CONVERT(date,dbo.Leads.createdon)
left JOIN     dbo.DimQuotations ON CONVERT(date, dateDim.TheDate)  = CONVERT(date,dbo.DimQuotations.DELIVERYDATE)
left JOIN     dbo.DimRentalOrder ON CONVERT(date, dateDim.TheDate)  = CONVERT(date,  dbo.DimRentalOrder.DateOnHire)
where dbo.Opportunities.opportunityid is not null or dbo.Leads.leadid is not null or dbo.DimQuotations.QUOTATIONID is not null or dbo.DimRentalOrder.[RentalOrder] is not null



GO
/****** Object:  View [dbo].[vw_LeadOpportunityLink]    Script Date: 2018-10-05 2:31:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_LeadOpportunityLink]
as

select opportunityid,originatingleadid,leads.ownerid from leads left join Opportunities on leadid = originatingleadid
union 
select opportunityid,originatingleadid,Opportunities.ownerid from Opportunities left join leads on leadid = originatingleadid


GO
/****** Object:  View [dbo].[vw_OpportunityQuoteLink]    Script Date: 2018-10-05 2:31:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE view [dbo].[vw_OpportunityQuoteLink]
as


--Date Opps
(SELECT dateDim.TheDate
      ,dateDim.[Year]
      ,dateDim.[Month]
      ,dateDim.[Week]
      ,dateDim.[Day]
      ,dateDim.[MonthName]
      ,dateDim.[DayName]
      ,dateDim.[DayOfYear]
      ,dateDim.[WeekDay]
      ,dateDim.[FriendlyDate]
      ,UserOpps.opportunityid
	  ,UserOpps.originatingleadid
	  ,null as 	leadid
	  ,null as 	QUOTATIONID
	  ,null as 	RentalOrder
	   ,null as accountid
	  ,UserOpps.CRMGUID   
	  ,UserOpps.EMPLOYEEID     
FROM          dbo.BI_DateDimension as dateDim 
left JOIN    (SELECT          AXUsers.EMPLOYEEID
                           ,AXUsers.CRMGUID
						   ,AXUsers.FULLNAME
                           ,dbo.Opportunities.opportunityid
						                              ,dbo.Opportunities.originatingleadid
						   ,CONVERT(date,dbo.Opportunities.createdon) as CreatedDate                           
                 FROM      dbo.CRM_AXUsers as AXUsers left JOIN  dbo.Opportunities ON AXUsers.CRMGUID = dbo.Opportunities.ownerid) as UserOpps ON CONVERT(date, dateDim.TheDate)  = UserOpps.CreatedDate)
union
--Date Leads 
(SELECT dateDim.TheDate
      ,dateDim.[Year]
      ,dateDim.[Month]
      ,dateDim.[Week]
      ,dateDim.[Day]
      ,dateDim.[MonthName]
      ,dateDim.[DayName]
      ,dateDim.[DayOfYear]
      ,dateDim.[WeekDay]
      ,dateDim.[FriendlyDate]  
	  ,null as opportunityid
	  ,null as 	originatingleadid
	  ,UserLeads.leadid	
	  ,null as 	QUOTATIONID
	  ,null as 	RentalOrder
	   ,null as accountid
	  ,UserLeads.CRMGUID     
	   ,UserLeads.EMPLOYEEID
FROM          dbo.BI_DateDimension as dateDim 
left JOIN     (SELECT       AXUsers.EMPLOYEEID
                           ,AXUsers.CRMGUID
						   ,AXUsers.FULLNAME
                           ,dbo.Leads.leadid
						   ,CONVERT(date,dbo.Leads.createdon) as CreatedDate                           
FROM                        dbo.CRM_AXUsers as AXUsers 
						   left JOIN  dbo.Leads ON AXUsers.CRMGUID = dbo.Leads.ownerid ) as UserLeads ON CONVERT(date, dateDim.TheDate)  = UserLeads.CreatedDate)
union

--Date Quotes
(SELECT dateDim.TheDate
      ,dateDim.[Year]
      ,dateDim.[Month]
      ,dateDim.[Week]
      ,dateDim.[Day]
      ,dateDim.[MonthName]
      ,dateDim.[DayName]
      ,dateDim.[DayOfYear]
      ,dateDim.[WeekDay]
      ,dateDim.[FriendlyDate]  	  
	  ,null as opportunityid
	  ,null as 	originatingleadid
	  ,null as leadid	
	 ,UserQuotations.QUOTATIONID
	  ,null as 	RentalOrder
	   ,null as accountid	   	
	  ,UserQuotations.CRMGUID
	    ,UserQuotations.EMPLOYEEID     
FROM          dbo.BI_DateDimension as dateDim 
left JOIN   (SELECT          AXUsers.EMPLOYEEID
                           ,AXUsers.CRMGUID
						   ,AXUsers.FULLNAME
                           ,dbo.DimQuotations.QUOTATIONID
						   ,CONVERT(date,dbo.DimQuotations.DELIVERYDATE) as CreatedDate                           
FROM                        dbo.CRM_AXUsers as AXUsers 
						   left JOIN  dbo.DimQuotations ON AXUsers.EMPLOYEEID = dbo.DimQuotations.SALESTAKER) as UserQuotations ON CONVERT(date, dateDim.TheDate)   = UserQuotations.CreatedDate)
union

--Date Rentall Orders
(SELECT dateDim.TheDate
      ,dateDim.[Year]
      ,dateDim.[Month]
      ,dateDim.[Week]
      ,dateDim.[Day]
      ,dateDim.[MonthName]
      ,dateDim.[DayName]
      ,dateDim.[DayOfYear]
      ,dateDim.[WeekDay]
      ,dateDim.[FriendlyDate]     
	  ,null as opportunityid
	  ,null as 	originatingleadid
	  ,null as leadid	
	  ,null as QUOTATIONID
	  ,UserRentalOrder.RentalOrder
	   ,null as accountid		
	  ,UserRentalOrder.CRMGUID 
	  ,UserRentalOrder.EMPLOYEEID    
FROM          dbo.BI_DateDimension as dateDim 
left JOIN  (SELECT          AXUsers.EMPLOYEEID
                           ,AXUsers.CRMGUID
						   ,AXUsers.FULLNAME
                           ,dbo.DimRentalOrder.RentalOrder
						   ,CONVERT(date,dbo.DimRentalOrder.DateOnHire) as CreatedDate                           
FROM                        dbo.CRM_AXUsers as AXUsers 
						   left JOIN  dbo.DimRentalOrder ON AXUsers.EMPLOYEEID = dbo.DimRentalOrder.KAMID)  as UserRentalOrder ON CONVERT(date, dateDim.TheDate)  = UserRentalOrder.CreatedDate)
union				  
--Date Account		  
	(SELECT dateDim.TheDate
      ,dateDim.[Year]
      ,dateDim.[Month]
      ,dateDim.[Week]
      ,dateDim.[Day]
      ,dateDim.[MonthName]
      ,dateDim.[DayName]
      ,dateDim.[DayOfYear]
      ,dateDim.[WeekDay]
      ,dateDim.[FriendlyDate]    	
	  ,null as opportunityid
	  ,null as 	originatingleadid
	  ,null as leadid	
	  ,null as QUOTATIONID
	  ,null as RentalOrder
	  ,UserAccounts.accountid
	  ,UserAccounts.CRMGUID 
	  	  ,UserAccounts.EMPLOYEEID    
FROM          dbo.BI_DateDimension as dateDim 
						   left JOIN    (SELECT          AXUsers.EMPLOYEEID
                           ,AXUsers.CRMGUID
						   ,AXUsers.FULLNAME
                           ,dbo.Accounts.accountid
						   ,CONVERT(date,dbo.Accounts.createdon) as CreatedDate                           
FROM                        dbo.CRM_AXUsers as AXUsers 
						   left JOIN  dbo.Accounts ON AXUsers.CRMGUID = dbo.Accounts.ownerid) as UserAccounts ON CONVERT(date, dateDim.TheDate)  = UserAccounts.CreatedDate)










GO
/****** Object:  View [dbo].[vw_RentalOrderQuoteLink]    Script Date: 2018-10-05 2:31:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_RentalOrderQuoteLink]
as


SELECT R.RENTALORDER,R.RentalOrderKey,Q.QUOTATIONID ,Q.SALESIDREF,Q.SALESRESPONSIBLE, Q.SALESTAKER,R.KAMID,KAMName,R.Customer,R.CustomerAccount,R.DateOnHire  FROM DimRentalOrder r
left join [CRM_OnlineData].[dbo].[DimQuotations] q on  R.RENTALORDER = Q.SALESIDREF



GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateCRMAXFactTable]    Script Date: 2018-10-05 2:31:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create proc [dbo].[sp_UpdateCRMAXFactTable] as

begin
; with a as (
SELECT  [EMPLOYEEID] = CASE
                                          WHEN S.maincontact <> '' THEN
                                          s.maincontact
                                          WHEN ( S.maincontact IS NULL
                                                  OR S.maincontact = '' )
                                               AND ( R.kamid IS NULL
                                                      OR R.kamid = '' ) THEN
                                          q.salesresponsible
                                          WHEN ( S.maincontact IS NULL
                                                  OR S.maincontact = '' )
                                               AND ( R.kamid IS NOT NULL
                                                      OR R.kamid <> '' ) THEN
                                            CASE
                                              WHEN R.kamid IS NOT NULL
                                                    OR R.kamid <> '' THEN
                                              r.kamid
                                            END
                                        END,
                         L.leadid,
                         o.opportunityid,
                         q.quotationid,
                         rentalorderkey,
                         O.originatingleadid,
                         CASE
                           WHEN R.dateonhire IS NOT NULL
                                 OR R.dateonhire <> '' THEN
                           Cast(R.dateonhire AS DATE)
                           WHEN R.dateonhire IS NULL
                                 OR R.dateonhire = '' THEN
                           Cast(Q.createddatetime AS DATE)
                           ELSE NULL
                         END AS 'THEDATE'
         FROM   [SQLBI].hgh_bi.dbo.dimrentalorder R
                LEFT JOIN [SQLBI].hgh_bi.dbo.dimquotations q
                       ON Q.salesidref = R.rentalorder               
                LEFT JOIN crm_onlinedata.dbo.opportunities o
                       ON O.hgh_quoteid = q.quotationid
                LEFT JOIN crm_onlinedata.dbo.leads L
                       ON O.originatingleadid = CONVERT(VARCHAR(50), L.leadid)
                LEFT JOIN [SQLBI].eazi_ax_production.dbo.smmbusreltable S
                       ON S.busrelaccount = R.customeraccount
             
         WHERE  r.dateonhire >= '2018-03-03'
                AND q.dataareaid = 'eaz'
                AND q.salesidref <> ''
         UNION
              -- All Quotations with no rental Orders --> Opportunities --> Leads
         SELECT  [EMPLOYEEID] = CASE
                                          WHEN S.maincontact <> '' THEN
                                          s.maincontact
                                          WHEN ( S.maincontact IS NULL
                                                  OR S.maincontact = '' ) THEN
                                          q.salesresponsible
                                        END,
                         L.leadid,
                         o.opportunityid,
                         q.quotationid,
                         RENTALORDERKEY =NULL,
                         O.originatingleadid,
                         CASE
                           WHEN Q.createddatetime IS NOT NULL
                                 OR q.createddatetime <> '' THEN Cast(
                           Q.createddatetime AS DATE)
                           WHEN Q.createddatetime IS NULL
                                 OR Q.createddatetime = '' THEN Cast(
                           Q.createddatetime AS DATE)
                           ELSE NULL
                         END AS 'THEDATE'
         FROM   [SQLBI].hgh_bi.dbo.dimquotations q
                LEFT JOIN crm_onlinedata.dbo.opportunities o
                       ON O.hgh_quoteid = q.quotationid
                LEFT JOIN crm_onlinedata.dbo.leads L
                       ON O.originatingleadid = CONVERT(VARCHAR(50), L.leadid)
                LEFT JOIN [SQLBI].eazi_ax_production.dbo.smmbusreltable S
                       ON S.busrelaccount = q.custaccount         
         WHERE  q.createddatetime >= '2018-03-03'
                AND q.dataareaid = 'eaz'
                AND q.salesidref = ''
         UNION
              -- All Opportunities with no Quotations and their --> Leads
         SELECT [EMPLOYEE] = CASE
                               WHEN O.ownerid IS NOT NULL THEN
                               (SELECT employeeid
                                FROM   crm_axusers
                                WHERE  employeeid = U.employeeid)
                               WHEN L.ownerid IS NOT NULL
                                    AND O.ownerid IS NULL THEN
                               (SELECT employeeid
                                FROM   crm_axusers
                                WHERE  employeeid = U.employeeid)
                             END,
                L.leadid,
                o.opportunityid,
                QUOTATIONID = NULL,
                RENTALORDERKEY =NULL,
                O.originatingleadid,

                CASE
                  WHEN Cast(o.createdon AS DATE) IS NOT NULL
                        OR Cast(o.createdon AS DATE) <> '' THEN
                  Cast(o.createdon AS DATE)
                  ELSE Cast(l.createdon AS DATE)
                END AS 'THEDATE'
         FROM   crm_onlinedata.dbo.opportunities o
                LEFT JOIN crm_onlinedata.dbo.leads L
                       ON O.originatingleadid = CONVERT(VARCHAR(50), L.leadid)
                LEFT JOIN crm_onlinedata.dbo.crm_axusers U
                       ON U.crmguid = O.ownerid
         WHERE  o.createdon >= '2018-03-03'
                AND O.hgh_quoteid = '' 
         UNION
              -- All Leads without links to opportunities etc
         SELECT [EMPLOYEE] = CASE
                               WHEN l.ownerid IS NOT NULL THEN
                              
                               (SELECT EMPLOYEEID
                                FROM   crm_axusers
                                WHERE  CRMGUID = l.ownerid)
                             END,
                L.leadid,
                OPPORTUNITYID,
                QUOTATIONID = NULL,
                RENTALORDERKEY =NULL,
                O.originatingleadid,
                           Cast(l.createdon AS DATE) as 'THEDATE'
         FROM   crm_onlinedata.dbo.leads L
                LEFT JOIN crm_onlinedata.dbo.opportunities o
                       ON O.originatingleadid = CONVERT(VARCHAR(50), L.leadid)
                LEFT JOIN crm_onlinedata.dbo.crm_axusers U
                       ON U.crmguid = O.ownerid
         WHERE  l.createdon >= '2018-03-03' AND CONVERT(VARCHAR(50), L.leadid) not in (Select originatingleadid from Opportunities)

)
Insert into CRMAXFactTable
Select * From a

End

GO
