SELECT [EMPLOYEE] = CASE 
                      WHEN S.maincontact <> '' THEN (SELECT fullname  FROM  crm_onlinedata.dbo.crm_axusers 
                      WHERE employeeid = s.maincontact) 
                      WHEN ( S.maincontact IS NULL 
                              OR S.maincontact = '' ) 
                           AND ( R.kamid IS NULL 
                                  OR R.kamid = '' ) 
                      THEN (SELECT fullname FROM 
                      crm_onlinedata.dbo.crm_axusers 
                      WHERE 
                      employeeid = q.salesresponsible) 
                      WHEN ( S.maincontact IS NULL 
                              OR S.maincontact = '' ) 
                           AND ( R.kamid IS NOT NULL 
                                  OR R.kamid <> '' ) THEN 
                        CASE 
                          WHEN R.kamid IS NOT NULL 
                             OR R.kamid <> '' THEN r.kamname 
                        END 
                    END, 
       R.[kamid], 
       R.[kamname], 
       CASE 
         WHEN R.customeraccount <> '' 
               OR R.customeraccount IS NOT NULL THEN R.customeraccount 
         WHEN R.customeraccount = '' 
               OR R.customeraccount IS NULL THEN Q.custaccount 
         ELSE NULL 
       END AS 'CUSTOMERACCOUNT', 
       RF.DATEKEY AS 'THEDATE', 
       R.[rentalorderkey],        --,R.[Customer], 
       R.[invoiceaccount], 
       R.[searchname], 
       R.[salestakercode], 
       R.[salestakername], 
       R.[contactemail], 
       R.[venue], 
       R.[venuegroup], 
       R.[salesresponsiblecode], 
       R.[salesresponsiblename], 
       R.[salesresponsiblefloor], 
       R.[industry],
       R.[DateOnHire] , 
       R.[rentalorderwarehouse], 
       R.[rentalorderwarehousedescription], 
       R.[kamfloor]
       
FROM   [SQLBI].HGH_BI.dbo.dimrentalorder R 
       LEFT JOIN [SQLBI].HGH_BI.dbo.dimquotations Q 
              ON Q.salesidref = R.rentalorder 
                 AND Q.dataareaid = 'eaz' 
      LEFT JOIN [SQLBI].HGH_BI.dbo.FACT_RENTALPERFORMANCE2 RF 
              ON R.rentalorderkey= RF.rentalorderkey 
                 AND R.rentalorderkey like 'eaz%' 

       LEFT JOIN [SQLBI].[Eazi_Ax_Production].dbo.[SMMBUSRELTABLE] S 
              ON S.busrelaccount = Q.custaccount 
                 AND S.dataareaid = 'com' 
WHERE  r.rentalorderkey LIKe 'eaz%' 
       AND Year(R.dateonhire) >= 2017 