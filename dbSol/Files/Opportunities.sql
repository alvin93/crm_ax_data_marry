SELECT distinct [EMPLOYEE] = CASE 
                        WHEN (S.MAINCONTACT IS NOT NULL OR S.MAINCONTACT <> '')
						AND (R.KAMID IS  NULL OR R.KAMID = '')
						AND (Q.SALESRESPONSIBLE IS NULL OR Q.SALESRESPONSIBLE = '') THEN (Select s.Maincontact from CRM_AXUsers where EMPLOYEEID = S.MAINCONTACT) 
						
						WHEN (R.KAMID IS NOT NULL OR R.KAMID <> '' )
						AND (S.MAINCONTACT IS  NULL OR S.MAINCONTACT = '')						
						AND (Q.SALESRESPONSIBLE IS NULL OR Q.SALESRESPONSIBLE = '') THEN (Select R.Kamid from CRM_AXUsers where EMPLOYEEID = R.KAMID) 

						WHEN (Q.SALESRESPONSIBLE IS NOT NULL OR Q.SALESRESPONSIBLE <> '')
						AND (S.MAINCONTACT IS  NULL OR S.MAINCONTACT = '')
						AND (R.KAMID IS  NULL OR R.KAMID = '') THEN (Select Fullname from CRM_AXUsers where EMPLOYEEID = Q.SALESRESPONSIBLE) 
						
						WHEN O.ownerid IS NOT NULL 
                             AND Q.salesresponsible IS NULL 
                             AND R.kamid IS NULL 
                             AND S.maincontact IS NULL THEN (Case when l.ownerid is not null then u.EMPLOYEEID end )
						
						WHEN L.ownerid IS NOT NULL
							 AND O.ownerid IS NULL 
                             AND Q.salesresponsible IS NULL 
                             AND R.kamid IS NULL 
                             AND S.maincontact IS NULL THEN (Case when l.ownerid is not null then U.employeeid end  ) 
                      END, 
					 
       o.[opportunityid],	
       o.[name], 
       o.[ownerid], 
       o.[ownerid_name], 
       o.[hgh_city], 
       o.[hgh_branch], 
       o.[hgh_branch_name], 
       o.[hgh_opportunity_sitename], 
       o.[createdon], 
       o.[createdby], 
       o.[hgh_rentalstartdate], 
       o.[createdby_name], 
       o.[modifiedby], 
       o.[modifiedby_name], 
       o.[originatingleadid], 
       o.[originatingleadid_name], 
       o.[hgh_quoteid], 
       o.[customerid], 
       o.[customerid_name], 
       o.[parentcontactid], 
       o.[parentcontactid_name], 
       o.[parentaccountid], 
       o.[parentaccountid_name], 
       o.[transactioncurrencyid], 
       o.[transactioncurrencyid_name], 
       o.[actualvalue], 
       o.[totalamount], 
       o.[actualvalue_base], 
       o.[hgh_axtotalprice_base], 
       o.[actualclosedate], 
       o.[processid], 
       o.[plain_processid], 
       o.[hgh_bussinesssiteid], 
       o.[hgh_bussinesssiteid_name], 
       o.[hgh_ordercomments], 
       o.[discountpercentage], 
       o.[salesstage], 
       o.[stepname], 
       o.[filedebrief], 
       o.[estimatedclosedate], 
       o.[pricelevelid], 
       o.[new_segment], 
       o.[finaldecisiondate], 
       o.[captureproposalfeedback], 
       o.[new_sector], 
       o.[hgh_contractortype_security], 
       o.[identifycompetitors], 
       o.[hgh_axtotalprice], 
       o.[hgh_rentalenddate], 
       o.[decisionmaker], 
       o.[hgh_adress1_composite], 
       o.[prioritycode], 
       o.[hgh_calendar], 
       o.[isrevenuesystemcalculated], 
       o.[completeinternalreview], 
       o.[hgh_contractortype_structuralsteel], 
       o.[confirminterest], 
       o.[presentproposal], 
       o.[hgh_latitude], 
       o.[proposedsolution], 
       o.[hgh_contractortype_electrical], 
       o.[hgh_contractortype_plumbing], 
       o.[identifycustomercontacts], 
       o.[hgh_contractortype_hvac], 
       o.[stageid], 
       o.[traversedpath], 
       o.[evaluatefit], 
       o.[totalamountlessfreight], 
       o.[totallineitemdiscountamount], 
       o.[hgh_contractortype], 
       o.[timezoneruleversionnumber], 
       o.[totaldiscountamount], 
       o.[statuscode], 
       o.[totalamountlessfreight_base], 
       o.[msdyn_ordertype], 
       o.[customerneed], 
       o.[totaltax_base], 
       o.[totallineitemamount_base], 
       o.[estimatedvalue], 
       o.[totalamount_base], 
       o.[developproposal], 
       o.[purchaseprocess], 
       o.[currentsituation], 
       o.[description], 
       o.[modifiedon], 
       o.[resolvefeedback], 
       o.[totaltax], 
       o.[totaldiscountamount_base], 
       o.[hgh_longitude], 
       o.[hgh_invoiceprofile], 
       o.[sendthankyounote], 
       o.[hgh_contractortype_claddingroofing], 
       o.[exchangerate], 
       o.[estimatedvalue_base], 
       o.[hgh_contractortype_painting], 
       o.[budgetamount_base], 
       o.[hgh_tequestforquote], 
       o.[presentfinalproposal], 
       o.[owninguser], 
       o.[hgh_contractortype_fireinstallation], 
       o.[budgetamount], 
       o.[pricingerrorcode], 
       o.[hgh_contractortype_signage], 
       o.[salesstagecode], 
       o.[totallineitemdiscountamount_base], 
       o.[hgh_contractortype_civils], 
       o.[modifiedonbehalfby], 
       o.[purchasetimeframe], 
       o.[identifypursuitteam], 
       o.[participatesinworkflow], 
       o.[statecode], 
       o.[owningbusinessunit], 
       o.[pursuitdecision], 
       o.[opportunityratingcode], 
       o.[totallineitemamount], 
       o.[completefinalproposal], 
       o.[leadexists] 
FROM   [CRM_OnlineData].[dbo].[opportunities] o 
       LEFT JOIN leads L 
              ON O.originatingleadid = CONVERT(VARCHAR(50), L.leadid) 
       LEFT JOIN [SQLBI].HGH_BI.dbo.dimquotations Q 
              ON Q.quotationid = o.hgh_quoteid 
                 AND Q.dataareaid = 'eaz' 
       LEFT JOIN [SQLBI].HGH_BI.dbo.dimrentalorder R 
              ON R.rentalorder = Q.salesidref 
                 AND rentalorderkey like 'eaz%' 
       LEFT JOIN  [SQLBI].[Eazi_Ax_Production].dbo.[SMMBUSRELTABLE] S 
              ON S.busrelaccount = Q.custaccount 
                 AND S.dataareaid = 'com' 
       LEFT JOIN crm_axusers U 
              ON U.crmguid = O.ownerid 
